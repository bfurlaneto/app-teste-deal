﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ClientesTesteDeal.Models
{
    public class Cliente
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(20)]
        public string CNPJ { get; set; }
        [Required]
        [StringLength(80)]
        public string Nome { get; set; }
        [Required]
        [StringLength(100)]
        public string Endereco { get; set; }
        [Required]
        public int Numero { get; set; }
        [Required]
        [StringLength(80)]
        public string Complemento { get; set; }
        [Required]
        [StringLength(10)]
        public string CEP { get; set; }
        
        public virtual List<Socio> Socios { get; set; }
        public virtual List<Telefone> Telefones { get; set; }
        public virtual List<Email> Emails { get; set; }
    }
}
