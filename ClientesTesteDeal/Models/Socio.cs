﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ClientesTesteDeal.Models
{
    public class Socio
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Cliente")]
        public int Cliente_Id { get; set; }
        [Required]
        [StringLength(80)]
        public string Nome { get; set; }
        [Required]
        public string CPF { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}
