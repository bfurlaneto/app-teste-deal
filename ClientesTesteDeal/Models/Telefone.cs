﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ClientesTesteDeal.Models
{
    public class Telefone
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int Tipo { get; set; }
        [Required]
        public int DDD { get; set; }
        [Required]
        public int Numero { get; set; }
        [ForeignKey("Cliente")]
        public int Cliente_Id { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}
