﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ClientesTesteDeal.Models
{
    public class Email
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Cliente")]
        public int Cliente_Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Email_Cliente { get; set; }

        public virtual Cliente Cliente { get; set; }
    }
}
