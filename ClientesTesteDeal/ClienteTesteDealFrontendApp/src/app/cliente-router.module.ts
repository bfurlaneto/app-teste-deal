import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClienteConsultaComponent } from './clientes/cliente-consulta/cliente-consulta.component';
import { ClienteEditaComponent } from './clientes/cliente-edita/cliente-edita.component';

const rotas: Routes = [
  { path: '', redirectTo: 'clientes', pathMatch: 'full' },
  { path: 'clientes', component: ClienteConsultaComponent },
  { path: 'cliente', component: ClienteEditaComponent },
  { path: 'cliente/:id', component: ClienteEditaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(rotas)],
  exports: [RouterModule]
})
export class ClienteRouterModule { }
