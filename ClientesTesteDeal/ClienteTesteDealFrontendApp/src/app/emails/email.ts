export interface Email {
  id: number;
  cliente_id: number;
  email: string;
}
