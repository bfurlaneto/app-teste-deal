import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Email } from '../email';

@Component({
  selector: 'email-edita',
  templateUrl: './email-edita.component.html',
  styleUrls: ['./email-edita.component.css']
})
export class EmailEditaComponent implements OnInit {

  @Output() emailAdicionado = new EventEmitter<Email>();

  constructor() { }

  ngOnInit() {
  }

  adicionarEmail(email: Email) {
    this.emailAdicionado.emit(email);
  }

}
