import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailEditaComponent } from './email-edita.component';

describe('EmailEditaComponent', () => {
  let component: EmailEditaComponent;
  let fixture: ComponentFixture<EmailEditaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailEditaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailEditaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
