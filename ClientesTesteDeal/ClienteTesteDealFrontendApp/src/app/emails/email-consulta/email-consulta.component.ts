import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'email-consulta',
  templateUrl: './email-consulta.component.html',
  styleUrls: ['./email-consulta.component.css']
})
export class EmailConsultaComponent implements OnInit {

  @Input() emails = [];
  @Output() emailDeletado = new EventEmitter<object>();

  constructor() { }

  ngOnInit() {
  }

  deletarEmail(email: object) {
    this.emailDeletado.emit(email);
  }

}
