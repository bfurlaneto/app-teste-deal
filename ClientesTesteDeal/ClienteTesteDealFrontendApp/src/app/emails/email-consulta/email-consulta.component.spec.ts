import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailConsultaComponent } from './email-consulta.component';

describe('EmailConsultaComponent', () => {
  let component: EmailConsultaComponent;
  let fixture: ComponentFixture<EmailConsultaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailConsultaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailConsultaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
