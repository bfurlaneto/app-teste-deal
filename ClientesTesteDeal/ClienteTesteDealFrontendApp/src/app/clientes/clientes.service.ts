import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cliente } from './cliente';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

const urlBase = "api/cliente";

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor(private http: HttpClient) { }

  getClientes(): Observable<Cliente[]> {
    const url = urlBase + "/clientes";
    return this.http.get<Cliente[]>(url);
  }

  getCliente(id: number): Observable<Cliente> {
    const url = urlBase + "?id=${id}";
    return this.http.get<Cliente>(url);
  }

  adicionarCliente(cliente: Cliente): Observable<Cliente> {
    const url = urlBase;
    return this.http.post<Cliente>(url, cliente, httpOptions);
  }

  atualizarCliente(cliente: Cliente): Observable<any> {
    const url = urlBase + "?id=${id}";
    return this.http.put(url, cliente, httpOptions);
  }

  excluirCliente(id: number): Observable<any> {
    const url = urlBase + "?id=${id}";
    return this.http.delete(url, httpOptions);
  }
}
