import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientesService } from '../clientes.service';
import { Cliente } from '../cliente';
import { Telefone } from '../../telefones/telefone';
import { Socio } from '../../socios/socio';
import { Email } from '../../emails/email';

@Component({
  selector: 'cliente-edita',
  templateUrl: './cliente-edita.component.html',
  styleUrls: ['./cliente-edita.component.css']
})
export class ClienteEditaComponent implements OnInit {

  cliente: Cliente;

  constructor(private rota: ActivatedRoute, private clienteService: ClientesService) { }

  ngOnInit() {
    this.pesquisarCliente();
  }

  pesquisarCliente(): void {
    const id = +this.rota.snapshot.paramMap.get('id');
    if (id) {
      this.clienteService.getCliente(id).subscribe(
        retorno => {
          this.cliente = retorno;
        });
    } else {
      this.cliente = {
        id: null,
        cnpj: '',
        cep: '',
        complemento: '',
        emails: [],
        endereco: '',
        nome: '',
        numero: null,
        socios: [],
        telefones: []
      };
    }

  }

  onTelefoneAdicionado(telefone: Telefone) {
    this.cliente.telefones.push(telefone);
  }

  onSocioAdicionado(socio: Socio) {
    this.cliente.socios.push(socio);
  }

  onEmailAdicionado(email: Email) {
    this.cliente.emails.push(email);
  }

  onSubmit() {
    if (this.cliente.id) {
      this.clienteService.atualizarCliente(this.cliente).subscribe();
    } else {
      this.clienteService.adicionarCliente(this.cliente).subscribe();
    }
  }

}
