import { Socio } from "../socios/socio";
import { Telefone } from "../telefones/telefone";
import { Email } from "../emails/email";

export interface Cliente {
  id: number;
  cnpj: string;
  nome: string;
  endereco: string;
  cep: string;
  numero: number;
  complemento: string;
  socios: Socio[];
  telefones: Telefone[];
  emails: Email[];
}
