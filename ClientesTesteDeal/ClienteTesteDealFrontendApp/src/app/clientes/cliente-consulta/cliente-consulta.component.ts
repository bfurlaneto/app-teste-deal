import { Component, OnInit } from '@angular/core';
import { ClientesService } from '../clientes.service';
import { Cliente } from '../cliente';

@Component({
  selector: 'cliente-consulta',
  templateUrl: './cliente-consulta.component.html',
  styleUrls: ['./cliente-consulta.component.css']
})
export class ClienteConsultaComponent implements OnInit {

  clientes: Cliente[];

  constructor(private service: ClientesService) { }

  ngOnInit() {
    this.pesquisarClientes();
  }

  pesquisarClientes() {
    this.service.getClientes().subscribe(
      retorno => {
        this.clientes = retorno;
    });
  }

  deletarCliente(cliente: Cliente) {
    this.service.excluirCliente(cliente.id).subscribe();
  }

}
