import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocioEditaComponent } from './socio-edita.component';

describe('SocioEditaComponent', () => {
  let component: SocioEditaComponent;
  let fixture: ComponentFixture<SocioEditaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocioEditaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocioEditaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
