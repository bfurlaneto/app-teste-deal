import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Socio } from '../socio';


@Component({
  selector: 'app-socio-edita',
  templateUrl: './socio-edita.component.html',
  styleUrls: ['./socio-edita.component.css']
})
export class SocioEditaComponent implements OnInit {

  @Output() socioAdicionado = new EventEmitter<Socio>();

  constructor() { }

  ngOnInit() {
  }

  adicionarSocio(socio: Socio) {
    this.socioAdicionado.emit(socio);
  }

}
