export interface Socio {
  id: number;
  cliente_id: number;
  cpf: string;
  nome: string;
}
