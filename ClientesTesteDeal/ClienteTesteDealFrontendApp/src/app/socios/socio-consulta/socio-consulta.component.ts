import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'socio-consulta',
  templateUrl: './socio-consulta.component.html',
  styleUrls: ['./socio-consulta.component.css']
})
export class SocioConsultaComponent implements OnInit {

  @Input() socios = [];
  @Output() socioDeletado = new EventEmitter<object>();

  constructor() { }

  ngOnInit() {
  }

  deletarSocio(socio: object) {
    this.socioDeletado.emit(socio);
  }

}
