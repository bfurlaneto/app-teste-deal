import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocioConsultaComponent } from './socio-consulta.component';

describe('SocioConsultaComponent', () => {
  let component: SocioConsultaComponent;
  let fixture: ComponentFixture<SocioConsultaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocioConsultaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocioConsultaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
