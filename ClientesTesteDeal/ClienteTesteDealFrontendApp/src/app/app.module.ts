import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ClienteConsultaComponent } from './clientes/cliente-consulta/cliente-consulta.component';
import { ClienteEditaComponent } from './clientes/cliente-edita/cliente-edita.component';
import { SocioConsultaComponent } from './socios/socio-consulta/socio-consulta.component';
import { SocioEditaComponent } from './socios/socio-edita/socio-edita.component';
import { TelefoneConsultaComponent } from './telefones/telefone-consulta/telefone-consulta.component';
import { TelefoneEditaComponent } from './telefones/telefone-edita/telefone-edita.component';
import { EmailConsultaComponent } from './emails/email-consulta/email-consulta.component';
import { EmailEditaComponent } from './emails/email-edita/email-edita.component';
import { ClienteRouterModule } from './/cliente-router.module';


@NgModule({
  declarations: [
    AppComponent,
    ClienteConsultaComponent,
    ClienteEditaComponent,
    SocioConsultaComponent,
    SocioEditaComponent,
    TelefoneConsultaComponent,
    TelefoneEditaComponent,
    EmailConsultaComponent,
    EmailEditaComponent
  ],
  imports: [
    BrowserModule,
    ClienteRouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
