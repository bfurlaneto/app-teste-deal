export interface Telefone {
  id: number;
  cliente_id: number;
  ddd: number;
  numero: string;
  tipo: number;
}
