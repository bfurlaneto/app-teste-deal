import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelefoneEditaComponent } from './telefone-edita.component';

describe('TelefoneEditaComponent', () => {
  let component: TelefoneEditaComponent;
  let fixture: ComponentFixture<TelefoneEditaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelefoneEditaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelefoneEditaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
