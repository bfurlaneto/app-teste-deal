import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Telefone } from '../telefone';

@Component({
  selector: 'app-telefone-edita',
  templateUrl: './telefone-edita.component.html',
  styleUrls: ['./telefone-edita.component.css']
})
export class TelefoneEditaComponent implements OnInit {

  @Output() telefoneAdicionado = new EventEmitter<Telefone>();

  constructor() { }

  ngOnInit() {
  }

  adicionarTelefone(telefone: Telefone) {
    this.telefoneAdicionado.emit(telefone);
  }
}
