import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'telefone-consulta',
  templateUrl: './telefone-consulta.component.html',
  styleUrls: ['./telefone-consulta.component.css']
})
export class TelefoneConsultaComponent implements OnInit {

  @Input() telefones = [];
  @Output() telefoneDeletado = new EventEmitter<object>();

  constructor() { }

  ngOnInit() {
  }

  deletarTelefone(telefone: object) {
    this.telefoneDeletado.emit(telefone);
  }

}
