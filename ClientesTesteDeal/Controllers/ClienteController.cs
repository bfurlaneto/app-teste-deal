﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientesTesteDeal.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClientesTesteDeal.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ClienteController : Controller
    {

        private readonly AppDbContext contexto;

        public ClienteController(AppDbContext contexto)
        {
            this.contexto = contexto;
        }

        [HttpGet]
        [Route("clientes")]
        public IEnumerable<Cliente> GetAll()
        {
            return contexto.Cliente.ToList();
        }

        
        [HttpGet("{id}")]
        public IActionResult GetClienteById(int id)
        {
            var item = contexto.Cliente.FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }
        
        // POST: api/Cliente
        [HttpPost]
        public IActionResult AddCliente([FromBody]Cliente value)
        {
            var cliente = new Cliente
            {
                CNPJ = value.CNPJ,
                Nome = value.Nome,
                Endereco = value.Endereco,
                CEP = value.CEP,
                Numero = value.Numero,
                Complemento = value.Complemento,
                Emails = new List<Email>(),
                Telefones = new List<Telefone>(),
                Socios = new List<Socio>()
            };

            cliente.Telefones.AddRange(value.Telefones);
            cliente.Socios.AddRange(value.Socios);
            cliente.Emails.AddRange(value.Emails);

            contexto.Add(cliente);
            contexto.SaveChanges();

            return Ok(new
            {
                message = "Ciente foi adicionado com sucesso."
            }); 
        }
        
        // PUT: api/Cliente/5
        [HttpPut("{id}")]
        public IActionResult UpdateCliente(int id, [FromBody]Cliente value)
        {
            var cliente = new Cliente
            {
                CNPJ = value.CNPJ,
                Nome = value.Nome,
                Endereco = value.Endereco,
                CEP = value.CEP,
                Numero = value.Numero,
                Complemento = value.Complemento,
                Emails = new List<Email>(),
                Telefones = new List<Telefone>(),
                Socios = new List<Socio>()
            };

            cliente.Telefones.AddRange(value.Telefones);
            cliente.Socios.AddRange(value.Socios);
            cliente.Emails.AddRange(value.Emails);

            contexto.Update(cliente);
            contexto.SaveChanges();

            return Ok(new
            {
                message = "Cliente foi atualizado com sucesso."
            });
            
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var contact = contexto.Cliente.FirstOrDefault(t => t.Id == id);
            if (contact == null)
            {
                return NotFound();
            }
            contexto.Cliente.Remove(contact);
            contexto.SaveChanges();
            return Ok(new
            {
                message = "Cliente foi excluido com sucesso."
            });
        }
    }
}
